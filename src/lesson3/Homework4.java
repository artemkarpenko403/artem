package lesson3;

import java.util.*;
import java.util.List;

    public class Homework4 {
        public static void main(String[] args) {
            //Median();
            //Queue();
            //HashMap();
            Accounting();
            // Queue();
        }


//Problem1
//Для двух отсортированных массивов nums1 и nums2  вернуть медианное значение
// двух отсортированных массивов.

        public static void Median() {
            List<Integer> nums1 = Arrays.asList(1, 5, 7, 3);
            List<Integer> nums2 = Arrays.asList(2, 4, 6);
            List<Integer> result = new ArrayList<>();
            int p1 = 0;
            int p2 = 0;
            while (p1 < nums1.size() && p2 < nums2.size()) {
                if (nums1.get(0) < nums2.get(0)) {
                    result.add(nums1.get(p1));
                    p1++;
                } else {
                    result.add(nums2.get(p2));
                    p2++;
                }
            }
            if (p1 < nums1.size()) {
                while (p1 < nums1.size()) {
                    result.add(nums1.get(p1));
                    p1++;
                }
            }
            if (p2 < nums2.size()) {
                while (p2 < nums2.size()) {
                    result.add(nums2.get(p2));
                    p2++;
                }


            }
            System.out.println(result);

        }

        //Problem2
        //В отделении банка работает K окон, в общей очереди стоит N человек, каждому из которых понадобится 10 минут на обслуживание.
        // Вывести всех посететителей, кто подойдет к окнам через M минут от начала смены.
//    public static void Queue() {
//        Queue<String> queue = new LinkedList<>();
//        queue.add("Миша");
//        queue.add("Петя");
//        queue.add("Катя");
//        queue.add("Игорь");
//
//
//    }


        //Problem3
        //Подсчитайте повторяющиеся элементы в массиве с помощью HashMap.
        private static void HashMap() {
            int[] a = {5, 2, 3, 2, 5, 2, 5};
            HashMap<Integer, Integer> Number = new HashMap<>();
            for (int x : a) {
                int newValue = Number.getOrDefault(x, 0) + 1;
                Number.put(x, newValue);
            }

            System.out.println(Number);
        }


        //Problem4
        //Посчитайте остаток продуктов на конец месяца, имея данные о количестве на начало месяца и ежедневный расход.


        private static void Accounting() {
//
            int days = java.time.LocalDate.now().lengthOfMonth();
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите общее количество картофеля: ");
            double Potato = scanner.nextDouble();
            System.out.println("Введите общее количество перца: ");
            double Pepper = scanner.nextDouble();
            System.out.println("Введите общее количество кукурузы: ");
            double Corn = scanner.nextDouble();
            System.out.println("Введите дневную норму картофеля: ");
            double Potato1 = scanner.nextDouble();
            System.out.println("Введите дневную норму перца: ");
            double Pepper1 = scanner.nextDouble();
            System.out.println("Введите дневную норму кукурузы: ");
            double Corn1 = scanner.nextDouble();
            double MonthPotato = (Potato - Potato1 * days);
            double MonthPepper = (Pepper - Pepper1 * days);
            double MonthCorn = (Corn - Corn1 * days);
            System.out.println("Остаток картофеля за месяц: " + MonthPotato);
            System.out.println("Остаток перца за месяц: " + MonthPepper);
            System.out.println("Остаток кукурузы за месяц: " + MonthCorn);

        }

    }

