package lesson2;
//Дано: переменная int count, начальное значение можно указать любое.
//
//Напишите программу, которая выводит на экран count в степени 10, если count является чётным числом, и count в степени 3, если count нечётное.


public class ProblemNumber_1 {
    public static void main(String[] args) {
        int count = 2;

        if (count % 2 == 0) {
            System.out.println("Число " + Math.pow(count, 10)+  " четное в степени 10");

        } else {
            {
                System.out.println("Число "+ Math.pow(count, 3)+  " Нечетное в степени 3");
            }


        }
    }
}