package lesson2;

//Дано: целочисленное n, начальное значение можно указать любое.
//Найти количество натуральных чисел, не превосходящих n и не делящихся ни на одно из чисел 2, 3, 5.
public class ProblemNumber2 {
    public static void main(String[] args) {
        int n = 1999;
        int sum = 0;
        for (int i = 0; i <= n; i++) {
            if (i % 2 == 0) {
                continue;
            }
            if (i % 3 == 0) {
                continue;
            }
            if (i % 5 == 0) {
                continue;
            }
            sum = sum + 1;
        }
        System.out.println(sum);
    }
}
