package lesson2;

//Дано: строка str, начальное значение может быть любое.
//
//Напишите программу, считающую количество цифр 1, 2, 3 в строке.
//
//Пример: str = "сегодня мы купили 1 яблоко, 1 грушу и 2 апельсина"
//
//Кол-во 1: 2
//
//Кол-во 2: 1
//
//Кол-во 3: 0
public class ProblemNumber5 {
    public static void main(String[] args) {
        String s = "сегодня мы купили 2 яблоко, 1 грушу и 0 апельсина";
        System.out.println(s);
        int count = 0;
        boolean hasNumber = false;
        for (int i = 0; i < s.length(); i++) {
            if (Character.isDigit(s.charAt(i))) {
                hasNumber = true;
            }
            if (!Character.isDigit(s.charAt(i)) && hasNumber) {
                count++;
                hasNumber = false;
            }
        }
        if (hasNumber) {
            count++;
        }
        System.out.println("Кол-во чисел в строке = " + count);
    }
}
