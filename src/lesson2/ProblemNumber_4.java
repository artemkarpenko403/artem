package lesson2;
//Дано: целочисленное число n, начальное значение может быть любое.
//Напишите программу, которая определяет: (можно сделать не все варианты, а выбрать понравившийся)
//а) количество цифр в нем;
//б) сумму его цифр;
//в) произведение его цифр;
//г) среднее арифметическое его цифр;
//д) сумму квадратов его цифр;
//е) сумму кубов его цифр;
//ж) его первую цифру;
//з) сумму его первой и последней цифр.

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public class ProblemNumber_4 {
    public static void main(String[] args) {
        //а) количество цифр в нем
        int n = 167868;
        int countOfNumbers = 0;
        for (; n != 0; n /= 10)
            ++countOfNumbers;
        System.out.println(countOfNumbers);

    }
}

//сумму его цифр;
class B {
    public static void main(String[] args) {
        int n = 167868;
        int x = n;
        int s = 0;
        while (x != 0) {
            s += x % 10;
            x /= 10;
            System.out.println("Сумма цифр в числе " + n + " = " + s);
        }
    }
}

//произведение его цифр;
class C {
    public static void main(String args[]) {
        int n[] = {5, 5, 5};
        double sum = 0;
        for (int x : n) {
            sum += x;
        }
        System.out.print("среднее арифметическое чисел равно: " + sum / n.length);
    }
}

////среднее арифметическое его цифр;
//class D {
//    public static void main(String[] args) {
//
//        float n = 22;
//        int length = args.length;
//
//        for(String i: args){
//            n += Float.parseFloat(i);
//        }
//        System.out.println(n);
//        float arthMean = n / length;
//        System.out.println(arthMean);
//
//
//    }
//}