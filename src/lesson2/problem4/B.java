package lesson2.problem4;

public class B {
    public static void main(String[] args) {
        int n = 167868;
        int x = n;
        int s = 0;
        while (x != 0) {
            s += x % 10;
            x /= 10;
            System.out.println("Сумма цифр в числе " + n + " = " + s);
        }
    }
}
