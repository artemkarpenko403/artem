package lesson2.problem4;

class C {
    public static void main(String args[]) {
        int n[] = {5, 5, 5};
        double sum = 0;
        for (int x : n) {
            sum += x;
        }
        System.out.print("среднее арифметическое чисел равно: " + sum / n.length);
    }
}
