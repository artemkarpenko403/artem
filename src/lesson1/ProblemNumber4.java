package lesson1;

//Выведите на экран диапазоны значений каждого из целочисленных примитивных типов.
public class ProblemNumber4 {
    public static void main(String[] args) {
        System.out.print("Byte ");
        System.out.print("MIN ");
        System.out.print(Byte.MIN_VALUE);
        System.out.print("  MAX ");
        System.out.println(Byte.MAX_VALUE);

        System.out.print("Short ");
        System.out.print("MIN ");
        System.out.print(Short.MIN_VALUE);
        System.out.print("  MAX ");
        System.out.println(Short.MAX_VALUE);

        System.out.print("Integer ");
        System.out.print("MIN ");
        System.out.print(Integer.MIN_VALUE);
        System.out.print("  MAX ");
        System.out.println(Integer.MAX_VALUE);

        System.out.print("Long ");
        System.out.print("MIN ");
        System.out.print(Long.MIN_VALUE);
        System.out.print("  MAX ");
        System.out.println(Long.MAX_VALUE);
    }
}
