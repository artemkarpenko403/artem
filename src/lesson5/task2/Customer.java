package lesson5.task2;

public class Customer {
    private String firstname;
    private String lastname;
    private String patronymic;
    private String address;
    private String cardNumber;
    private String bankAccount;

    public Customer(String firstname, String lastname, String patronymic, String address, String cardNumber, String bankAccount) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.patronymic = patronymic;
        this.address = address;
        this.cardNumber = cardNumber;
        this.bankAccount = bankAccount;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAddress() {
        return address;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public String getPatronymic() {
        return patronymic;
    }
}






