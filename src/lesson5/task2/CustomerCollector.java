package lesson5.task2;

import java.util.LinkedList;
import java.util.List;

public class CustomerCollector {
    private List<Customer> customers;
    public List<Customer> customersFilteredByCardNumber;

    public CustomerCollector(List<Customer> customers) {
        this.customers = customers;
    }

    public List<Customer> getFilteredCustomers(char number, int position) {
        customersFilteredByCardNumber = new LinkedList<>();

        for (Customer customer : customers) {
            if (customer.getCardNumber().charAt(position) == number) {
                customersFilteredByCardNumber.add(customer);
            }
        }
        return customersFilteredByCardNumber;
    }

    public void printAddressForFilteredCustomers(char number, int position) {
        List<Customer> filteredCustomers = getFilteredCustomers(number, position);

        for (Customer customer : filteredCustomers) {
            System.out.println("Адреса всех покупателей, у кого первая цифра номера кредитки 5 " + customer.getAddress());
        }

    }

}
