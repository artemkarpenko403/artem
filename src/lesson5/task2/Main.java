package lesson5.task2;

import java.util.Arrays;
import java.util.List;

/**
 * Создать класс покупатель с полями: фамилия, имя, отчество, адрес, номер кредитной карточки, номер банковского счета.
 * Создать список из 5 покупателей (в main).
 * Вывести:
 * Имя покупателя с самой длинной фамилией
 * Адреса всех покупателей, у кого первая цифра номера кредитки 5.
 * Всех покупателей с отчеством "Евгеньевич"
 */
public class Main {
    public static void main(String[] args) {
        Customer customer1 = new Customer("Дарья", "Семенова", "Ивановна", "Москва", "2020304078777336", "038560568072");
        Customer customer2 = new Customer("Анастасия", "Тарасова", "Кирилловна", "Коломна", "5020304078777676", "038999568072");
        Customer customer3 = new Customer("Кирилл", "Попов", "Дмитриевич", "Белгород", "2022356778777673", "695432568072");
        Customer customer4 = new Customer("Александр", "Константинова", "Евгеньевич", "Тула", "5020304073965271", "563560568072");
        Customer customer5 = new Customer("Оливия", "Коломна", "Евгеньевич", "Борисовка", "5030304078777676", "538683456072");

        List<Customer> customers = Arrays.asList(customer1, customer2, customer3, customer4, customer5);
//Имя покупателя с самой длинной фамилией
        int currentMax = 0;
        Customer currentMaxLengthLastnameCustomer = null;
        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getLastname().length() > currentMax) {
                currentMax = customers.get(i).getLastname().length();
                currentMaxLengthLastnameCustomer = customers.get(i);
                System.out.println("Имя покупателя с самой длинной фамилией " + currentMaxLengthLastnameCustomer.getFirstname());
            }
        }


        // Адреса всех покупателей, у кого первая цифра номера кредитки 5.
        CustomerCollector customerCollector = new CustomerCollector(customers);
        customerCollector.printAddressForFilteredCustomers('5', 0);
//Всех покупателей с отчеством "Евгеньевич"
        int currentUser = 0;
        Customer currentEvgevevich = null;
        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getPatronymic().equals("Евгеньевич")) {
                currentUser = customers.get(i).getLastname().length();
                currentEvgevevich = customers.get(i);
                System.out.println("Всех покупателей с отчеством 'Евгеньевич' " + currentEvgevevich.getFirstname());
            }
        }



    }
}




