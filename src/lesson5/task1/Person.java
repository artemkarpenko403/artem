package lesson5.task1;


public class Person {
    private String  fullName;
    private Integer age;
    public Person(String fullName, Integer age) {
        this.fullName = fullName;
        this.age = age;
    }
    public Person(){
    }
   public void talk(){
        System.out.println("Говорит "+this.fullName+", ему  "+ this.age+" лет");
   }
    public void move(){
        System.out.println("Переехал "+this.fullName+", ему "+ this.age+ " лет");
    }
}
