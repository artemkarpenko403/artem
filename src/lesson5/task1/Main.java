package lesson5.task1;

/**
 * Создать класс Person, который содержит:
 * a) поля fullName, age.
 * б) методы move() и talk(), в которых просто вывести на консоль сообщение -"Такой-то  Person говорит".
 * в) Добавьте два конструктора  - Person() и Person(fullName, age).
 * Создайте два объекта этого класса (в методе main). Один объект инициализируется конструктором Person(), другой - Person(fullName, age).
 */
public class Main {
    public static void main(String[] args) {
        Person noNameStudent = new Person();
        Person NameStudent = new Person("Timi ", 21);
        noNameStudent.talk();
        noNameStudent.move();
        NameStudent.move();
        NameStudent.talk();
    }
}
