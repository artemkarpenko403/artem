package Lesson6.task1;

import java.util.ArrayList;
import java.util.List;

/**
 * Напишите общий метод для подсчета количества элементов в коллекции, обладающих определенным свойством:
 * <p>
 * 1) нечетные целые числа
 * <p>
 * 2) простые числа
 */

public class CountingTheNumberOfElements {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 33, 443, 23, 11, 5, 178, 45, 645);
        Integer filtrationEvenNumbers = filtrationEvenNumbers(list);
        System.out.println(filtrationEvenNumbers);
        Integer filtrationOddNumbers = filtrationOddNumbers(list);
        System.out.println(filtrationOddNumbers);
    }

    private static <Type> Integer filtrationEvenNumbers(List<? extends Number> group) {
        List<Type> filtrationEvenNumbers = new ArrayList();
        group.forEach((element) -> {
            if (element.doubleValue() % 2 == 0) {
                filtrationEvenNumbers.add((Type) element);
            }
        });
        return filtrationEvenNumbers.size();
    }

    private static <Type> Integer filtrationOddNumbers(List<? extends Number> group) {
        List<Type> filtrationOddNumbers = new ArrayList();
        group.forEach((element) -> {
            if (element.doubleValue() % 2.0D != 0.0D) {
                filtrationOddNumbers.add((Type) element);
            }
        });
        return filtrationOddNumbers.size();
    }
}
