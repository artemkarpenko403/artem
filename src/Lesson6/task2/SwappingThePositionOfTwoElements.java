package Lesson6.task2;

import java.util.ArrayList;
import java.util.List;

/**
 * Напишите общий метод для обмена позициями двух разных элементов в массиве.
 */
public class SwappingThePositionOfTwoElements {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        list = filtrationEvenNumbers(list, 0, 2);

        for (Integer list1 : list) {
            System.out.print(list1 + " ");
        }
    }

    private static ArrayList<Integer> filtrationEvenNumbers(List<? extends Number> sort, int a, int b) {
        ArrayList<Integer> sortNew = (ArrayList<Integer>) sort;
        Integer temporary = sortNew.get(a);
        sortNew.set(a, sortNew.get(b));
        sortNew.set(b, temporary);
        return sortNew;
    }

}
